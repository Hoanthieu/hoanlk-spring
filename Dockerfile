FROM maven:3.9.3 as build
ENV HOME=/app
RUN mkdir -p $HOME
WORKDIR $HOME
ADD pom.xml $HOME
RUN mvn verify --fail-never
ADD . $HOME
RUN mvn package

FROM openjdk:17
COPY --from=build /app/target/*.jar /app/backend.jar

WORKDIR /app

CMD ["java", "-Xmx512m", "-jar","/app/backend.jar"]

EXPOSE 8080
