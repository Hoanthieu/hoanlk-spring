create function any_in_set(set1 character varying, set2 character varying) returns boolean
    language plpgsql
        as
        $$
declare i int default 0;
        c varchar(128);
        campos1 int;
BEGIN
        if (set2 is null) then return false;
end if;
        campos1 := length(set1) - length(replace(set1, ',', '')) + 1;
        i := 1;
        while i <= campos1 loop
        c := split_part(split_part(set1, ',', i), ',', 1);
        if c = ANY (string_to_array(set2,',')) then return true;
end if;
        i := i + 1;
end loop;
return false;
END;
        $$;

alter function any_in_set(varchar, varchar) owner to eshop;
