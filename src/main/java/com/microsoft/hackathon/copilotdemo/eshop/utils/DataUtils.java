package com.microsoft.hackathon.copilotdemo.eshop.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.Locale;

public final class DataUtils {
    public static String makeLikeParam(String s) {
        if (StringUtils.isEmpty(s)) {
            return null;
        }

        s = s.trim().toLowerCase(Locale.ROOT);
        return "%" + s + "%";
    }
}
