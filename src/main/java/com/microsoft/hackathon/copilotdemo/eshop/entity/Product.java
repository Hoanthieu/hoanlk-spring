package com.microsoft.hackathon.copilotdemo.eshop.entity;

import com.microsoft.hackathon.copilotdemo.eshop.dto.ProductDTO;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

@Entity
@Table(name = "tbl_product")
@Data
@NoArgsConstructor
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;
    private String tagLine;
    private String firstBrewed;
    private String description;
    private String imageUrl;
    private Double price;
    private Double abv;
    private Double ibu;
    private Integer targetFg;
    private Double targetOg;
    private Integer ebc;
    private Double srm;
    private Double ph;
    private Double attenuationLevel;
    private String volumeJsonStr;
    private String boilVolumeJsonStr;
    private String foodPairingJsonStr;
    private String brewersTips;
    private String contributedBy;
}
