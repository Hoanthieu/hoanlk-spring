package com.microsoft.hackathon.copilotdemo.eshop.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ProductSearchDTO {
    private String name;
    @JsonProperty("tag_line")
    private String tagline;
    private String description;
    @JsonProperty("price_min")
    private Double priceMin;
    @JsonProperty("price_max")
    private Double priceMax;
    @JsonProperty("food_pairing")
    private String foodPairing;
}
