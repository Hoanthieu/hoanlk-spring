package com.microsoft.hackathon.copilotdemo.eshop.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class CreateEntityWithImageRequest<T> {
    private MultipartFile file;
    private T model;
}
