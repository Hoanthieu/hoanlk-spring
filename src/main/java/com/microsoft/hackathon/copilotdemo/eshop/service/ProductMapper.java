package com.microsoft.hackathon.copilotdemo.eshop.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.hackathon.copilotdemo.eshop.dto.ProductDTO;
import com.microsoft.hackathon.copilotdemo.eshop.dto.Volume;
import com.microsoft.hackathon.copilotdemo.eshop.entity.Product;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductMapper {
    private final ObjectMapper objectMapper;
    @SneakyThrows
    public Product toEntity(ProductDTO dto) {
        Product entity = new Product();
        BeanUtils.copyProperties(dto, entity);
        if (dto.getVolume() != null) {
            String volumeJsonStr = objectMapper.writeValueAsString(dto.getVolume());
            entity.setVolumeJsonStr(volumeJsonStr);
        }
        if (dto.getBoilVolume() != null) {
            String boilVolumeJsonStr = objectMapper.writeValueAsString(dto.getBoilVolume());
            entity.setBoilVolumeJsonStr(boilVolumeJsonStr);
        }
        if (dto.getFoodPairing() != null) {
            String foodPairingJsonStr = dto.getFoodPairing()
                    .stream()
                    .collect(Collectors.joining(","));
            entity.setFoodPairingJsonStr(foodPairingJsonStr);
        }
        return entity;
    }

    public List<ProductDTO> toDto(List<Product> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public ProductDTO toDto(Product entity) {
        ProductDTO dto = new ProductDTO();
        BeanUtils.copyProperties(entity, dto);
        if (entity.getVolumeJsonStr() != null) {
            Volume volume = objectMapper.readValue(dto.getVolumeJsonStr(), Volume.class);
            dto.setVolume(volume);
        }
        if (entity.getBoilVolumeJsonStr() != null) {
            Volume boilVolume = objectMapper.readValue(entity.getBoilVolumeJsonStr(), Volume.class);
            dto.setBoilVolume(boilVolume);
        }
        if (entity.getFoodPairingJsonStr() != null) {
            List<String> foodPairing = Arrays.stream(
                    entity.getFoodPairingJsonStr().split(",")
            ).collect(Collectors.toList());
            dto.setFoodPairing(foodPairing);
        }
        return dto;
    }

    public List<Product> toEntity(List<ProductDTO> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }
}
