package com.microsoft.hackathon.copilotdemo.eshop.repository;

import com.microsoft.hackathon.copilotdemo.eshop.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Integer> {
    @Query("""
        SELECT product FROM Product product
        WHERE 1=1
        AND (:name IS NULL OR lower(product.name) LIKE :name)
        AND (:description IS NULL OR lower(product.description) LIKE :description)
        AND (:tagLine IS NULL OR lower(product.tagLine) LIKE :tagLine)
        AND (:foodPairing IS NULL OR lower(product.foodPairingJsonStr) LIKE :foodPairing)
        AND (:priceMin IS NULL OR product.price >= :priceMin)
        AND (:priceMax IS NULL OR product.price <= :priceMax)
        ORDER BY product.id DESC
""")
    Page<Product> search(
            @Param("name") String name,
            @Param("description") String description,
            @Param("tagLine") String tagLine,
            @Param("foodPairing") String foodPairing,
            @Param("priceMin") Double priceMin,
            @Param("priceMax") Double priceMax,
            Pageable pageable
    );
}
