package com.microsoft.hackathon.copilotdemo.eshop.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.hackathon.copilotdemo.eshop.dto.CreateEntityWithImageRequest;
import com.microsoft.hackathon.copilotdemo.eshop.dto.ProductDTO;
import com.microsoft.hackathon.copilotdemo.eshop.dto.ProductSearchDTO;
import com.microsoft.hackathon.copilotdemo.eshop.entity.Product;
import com.microsoft.hackathon.copilotdemo.eshop.repository.ProductRepository;
import com.microsoft.hackathon.copilotdemo.eshop.utils.FileUtil;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.DataInput;
import java.util.List;
import java.util.stream.Collectors;

import static com.microsoft.hackathon.copilotdemo.eshop.constant.FileContentTypes.IMAGE_TYPES;
import static com.microsoft.hackathon.copilotdemo.eshop.constant.FileContentTypes.PRODUCT_UPLOAD_FOLDER;
import static com.microsoft.hackathon.copilotdemo.eshop.utils.DataUtils.makeLikeParam;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class ProductService {
    private final ProductRepository repository;
    private final ProductMapper productMapper;
    private final ObjectMapper objectMapper;
    private final FileUtil fileUtil;
//    @SneakyThrows
//    public ProductDTO save(CreateEntityWithImageRequest<ProductDTO> request) {
//        ProductDTO productDTO = objectMapper.readValue((DataInput) request.getModel(), ProductDTO.class);
//
//        Product product = productMapper.toEntity(productDTO);
//
//        MultipartFile file = request.getFile();
//
//        boolean isImageFile = fileUtil.checkFileContentType(file, IMAGE_TYPES);
//
//        if (!isImageFile) {
//            throw new IllegalArgumentException("Upload image file only");
//        }
//
//        String filePath = fileUtil.saveFile(PRODUCT_UPLOAD_FOLDER, file);
//
//        product.setImageUrl(filePath);
//
//        log.info("[SERVICE] request to save a Product: {}", product);
//        product = repository.save(product);
//
//        return productMapper.toDto(product);
//    }

    public ProductDTO save(ProductDTO productDTO) {
        Product product = productMapper.toEntity(productDTO);
        log.info("[SERVICE] request to save a Product: {}", product);
        product = repository.save(product);
        return productMapper.toDto(product);
    }

    public Page<ProductDTO> search(ProductSearchDTO searchParams, Pageable pageable) {
        log.info("[SERVICE] request to get search Products by params: {}, pageable: {}", searchParams, pageable);
        Page<Product> productPage = repository.search(
                makeLikeParam(searchParams.getName()),
                makeLikeParam(searchParams.getDescription()),
                makeLikeParam(searchParams.getTagline()),
                makeLikeParam(searchParams.getFoodPairing()),
                searchParams.getPriceMin(),
                searchParams.getPriceMax(),
                pageable
        );
        List<ProductDTO> products = productMapper.toDto(productPage.getContent());
        return new PageImpl<>(products, pageable, productPage.getTotalElements());
    }

    public void deleteById(Integer id) {
        log.info("[SERVICE] request to delete product by id: {}", id);
        repository.deleteById(id);
    }

    public ProductDTO findById(Integer id) {
        log.info("[SERVICE] request to find product by id: {}", id);
        Product product = repository.findById(id).orElseThrow(() -> new EntityNotFoundException("Product with id: " + id + " not found"));
        return productMapper.toDto(product);
    }


}
