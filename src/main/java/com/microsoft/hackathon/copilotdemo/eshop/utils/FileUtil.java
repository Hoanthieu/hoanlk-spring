package com.microsoft.hackathon.copilotdemo.eshop.utils;

import jakarta.activation.MimetypesFileTypeMap;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;


/*
 * @author: TuanNA
 * @since: 7/8/2023 9:03 PM
 * @description:  Define Upload File Util
 * @update:
 *
 * */
@RequiredArgsConstructor
@Component
@Slf4j
public final class FileUtil {

    @Value("${upload.path}")
    private String uploadPath;

    public String saveFile(String folderCode, MultipartFile multipartFile) {
        log.info("Request to save a new file: {} to folder: {}",
                multipartFile.getOriginalFilename(),
                folderCode);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String subFolder = sdf.format(new Date());
        long subTime = System.currentTimeMillis();
        String filePathToSave = "/" + subFolder + "/" + subTime + "/" + folderCode + "/";
        String folderPath = uploadPath + filePathToSave;

        var name = Objects.requireNonNull(multipartFile.getOriginalFilename()).replace(" ", "_");
        var fileNameAndPath = Paths.get(folderPath, name);

        try {
            if (!fileNameAndPath.normalize().startsWith(folderPath)) {
                throw new IOException("Could not upload file: " + name);
            }
            Files.write(fileNameAndPath, multipartFile.getBytes());
            log.info("Save a new file: {} to folder: {} successfully",
                    multipartFile.getOriginalFilename(),
                    folderCode);

            return filePathToSave + multipartFile.getOriginalFilename();
        } catch (IOException e) {
            log.error("Save file has an error", e);
        }
        throw new RuntimeException(
                String.format("Save a new file: %s to folder: %s failed",
                        multipartFile.getOriginalFilename(),
                        folderCode)
        );
    }

    public boolean checkFileContentType(MultipartFile file, String[] validTypes) {
        log.info("Request to check content type of a new file: {} by valid types: {}",
                file.getOriginalFilename(),
                validTypes);
        MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
        String mimeType = fileTypeMap.getContentType(file.getName());
        return Arrays.asList(validTypes).contains(mimeType);
    }

}

