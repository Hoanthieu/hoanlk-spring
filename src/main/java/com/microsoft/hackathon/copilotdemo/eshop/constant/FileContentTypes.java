package com.microsoft.hackathon.copilotdemo.eshop.constant;

public class FileContentTypes {
    public static final String[] IMAGE_TYPES = new String[]{
            "image/jpeg",
            "image/png",
    };
    public static final String PRODUCT_UPLOAD_FOLDER = "product";
}
