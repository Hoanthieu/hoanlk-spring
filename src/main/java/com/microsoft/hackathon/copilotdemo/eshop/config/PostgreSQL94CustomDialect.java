package com.microsoft.hackathon.copilotdemo.eshop.config;

import org.hibernate.boot.model.FunctionContributions;
import org.hibernate.boot.model.FunctionContributor;
import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.type.BasicTypeReference;
import org.hibernate.type.SqlTypes;

public class PostgreSQL94CustomDialect implements FunctionContributor {
    private static final BasicTypeReference<Boolean> BOOLEAN_BASIC_TYPE_REFERENCE = new BasicTypeReference<>("boolean", Boolean.class, SqlTypes.BOOLEAN);
    @Override
    public void contributeFunctions(FunctionContributions functionContributions) {
        functionContributions
                .getFunctionRegistry()
                .register("any_in_set", new StandardSQLFunction("any_in_set", BOOLEAN_BASIC_TYPE_REFERENCE));
    }
}
