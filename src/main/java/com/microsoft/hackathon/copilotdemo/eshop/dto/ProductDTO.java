package com.microsoft.hackathon.copilotdemo.eshop.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Data
public class ProductDTO {
    private Integer id;
    private String name;
    @JsonProperty("tag_line")
    private String tagLine;
    @JsonProperty("first_brewed")
    private String firstBrewed;
    private String description;
    @JsonProperty("image_url")
    private String imageUrl;
    private Double price;
    private Double abv;
    private Double ibu;
    @JsonProperty("target_fg")
    private Integer targetFg;
    @JsonProperty("target_og")
    private Double targetOg;
    private Integer ebc;
    private Double srm;
    private Double ph;
    @JsonProperty("attenuation_level")
    private Double attenuationLevel;
    @JsonIgnore
    private String volumeJsonStr;
    @JsonProperty("volume")
    private Volume volume;
    @JsonProperty("boil_volume")
    private Volume boilVolume;
    @JsonIgnore
    private String foodPairingJsonStr;
    @JsonProperty("food_pairing")
    private List<String> foodPairing;
    @JsonProperty("brewers_tips")
    private String brewersTips;
    @JsonProperty("contributed_by")
    private String contributedBy;
}
