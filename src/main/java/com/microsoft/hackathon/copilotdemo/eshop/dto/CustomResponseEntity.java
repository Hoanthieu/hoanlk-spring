package com.microsoft.hackathon.copilotdemo.eshop.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CustomResponseEntity<T> {
    private Integer code;
    private T data;
    private List<String> errors;
    private Long offset;
    private Integer size;
    private Long totalElements;

}
