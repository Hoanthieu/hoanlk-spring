package com.microsoft.hackathon.copilotdemo.eshop.exception;


import com.microsoft.hackathon.copilotdemo.eshop.dto.CustomResponseEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
@Slf4j
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<CustomResponseEntity<Object>> handleAllExceptions(Exception ex, WebRequest request) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        log.error("", ex);
        return ResponseEntity.ok(
                CustomResponseEntity.builder()
                        .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                        .data(null)
                        .errors(details)
                        .build()
        );
    }

}
