package com.microsoft.hackathon.copilotdemo.eshop.dto;

import lombok.Data;

@Data
public class Volume {
    private Integer value;
    private String unit;
}
