package com.microsoft.hackathon.copilotdemo.eshop.controller;

import com.microsoft.hackathon.copilotdemo.eshop.dto.CreateEntityWithImageRequest;
import com.microsoft.hackathon.copilotdemo.eshop.dto.CustomResponseEntity;
import com.microsoft.hackathon.copilotdemo.eshop.dto.ProductDTO;
import com.microsoft.hackathon.copilotdemo.eshop.dto.ProductSearchDTO;
import com.microsoft.hackathon.copilotdemo.eshop.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/v1/products")
@RequiredArgsConstructor
@CrossOrigin
public class ProductController {
    private final ProductService service;

    @PostMapping
    public CustomResponseEntity<ProductDTO> createProductSingle(
            @RequestBody ProductDTO productDTO
    ) {
        ProductDTO product = service.save(productDTO);
        return CustomResponseEntity.<ProductDTO> builder()
                .code(HttpStatus.CREATED.value())
                .data(product)
                .errors(Collections.emptyList())
                .build();
    }

    @PostMapping("/search")
    public CustomResponseEntity<List<ProductDTO>> searchProduct(@RequestBody ProductSearchDTO searchParams, Pageable pageable) {
        Page<ProductDTO> productPage = service.search(searchParams, pageable);
        return CustomResponseEntity.<List<ProductDTO>> builder()
                .code(HttpStatus.OK.value())
                .data(productPage.getContent())
                .offset(pageable.getOffset())
                .size(pageable.getPageSize())
                .totalElements(productPage.getTotalElements())
                .errors(Collections.emptyList())
                .build();
    }
    @GetMapping("/{id}")
    public CustomResponseEntity<ProductDTO> findById(@PathVariable Integer id) {
        ProductDTO productDTO = service.findById(id);
        return CustomResponseEntity.<ProductDTO> builder()
                .code(HttpStatus.OK.value())
                .data(productDTO)
                .errors(Collections.emptyList())
                .build();
    }
    @DeleteMapping("/{id}")
    public CustomResponseEntity<String> deleteById(@PathVariable Integer id) {
        service.deleteById(id);
        return CustomResponseEntity.<String> builder()
                .code(HttpStatus.OK.value())
                .data("Product with id: " + id + " has been deleted")
                .errors(Collections.emptyList())
                .build();
    }
}
